/*
 * ec16squareroot.h
 *
 *  Created on: 28.06.2016
 *      Author: KREUTZUL
 */

#ifndef EC16FIXEDPOINT_EC16SQUAREROOT_H_
#define EC16FIXEDPOINT_EC16SQUAREROOT_H_


#include "ec16utilities.h"

#define ECLIB_SQRT_16(res, par) \
		ECLIB_Sqrt_16( ECLIB_pass_fix16(&res), ECLIB_pass_fix16(par))


void ECLIB_Sqrt_16(ECLIB_rcv_fix16(*res), ECLIB_rcv_fix16(par));


#endif /* EC16FIXEDPOINT_EC16SQUAREROOT_H_ */
