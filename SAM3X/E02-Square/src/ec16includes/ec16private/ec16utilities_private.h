/*
 * ec16utilities_private.h
 *
 *  Created on: 21.03.2017
 *      Author: KREUTZUL
 */

#ifndef EC16INCLUDES_EC16PRIVATE_EC16UTILITIES_PRIVATE_H_
#define EC16INCLUDES_EC16PRIVATE_EC16UTILITIES_PRIVATE_H_


#include "ec16datatypes.h"
#include "ec16defines.h"





static inline ECLIB_Bool ECLIB_bool_IsEven_s16_16(s16 number) {
	if ((number % 2) == 0) {
		return ECLIB_TRUE;
	} else {
		return ECLIB_FALSE;
	}
}

static inline ECLIB_Bool ECLIB_bool_IsInfinity_s16_16(s16 number) {
	if (number == ECLIB_S16_POS_INF) {
		return ECLIB_TRUE;
	}
	if (number == ECLIB_S16_NEG_INF) {
		return ECLIB_TRUE;
	}
	return ECLIB_FALSE;
}

static inline ECLIB_Bool ECLIB_bool_IsNAN_s16_16(s16 number) {
	if (number == ECLIB_S16_NAN) {
		return ECLIB_TRUE;
	}
	return ECLIB_FALSE;
}



static inline ECLIB_Bool ECLIB_bool_IsInfinity_s32_16(s32 number) {
	if (number == ECLIB_S32_POS_INF) {
		return ECLIB_TRUE;
	}
	if (number == ECLIB_S32_NEG_INF) {
		return ECLIB_TRUE;
	}
	return ECLIB_FALSE;
}

static inline ECLIB_Bool ECLIB_bool_IsNAN_s32_16(s32 number) {
	if (number == ECLIB_S32_NAN) {
		return ECLIB_TRUE;
	}
	return ECLIB_FALSE;
}


s8 ECLIB_s8_LimitTos8_s32_16(s32 par);
s16 ECLIB_s16_LimitTos16_s32_16(s32 par);
s32 ECLIB_s32_LimitTos32_u32_16(u32 par);

s16 ECLIB_s16_Shift_s16_16(s16 par, s8 sf);
s32 ECLIB_s32_Shift_s16_16(s16 par, s8 sf);
s32 ECLIB_s32_Shift_s32_16(s32 par, s8 sf);

s16 ECLIB_s16_ShiftLimitTos16_s32_16(s32 par, s8 sf);
s16 ECLIB_s16_ShiftLimitTos16_u32_16(u32 par, s8 sf);

s32 ECLIB_s32_Abs_s32_16(s32 par);

#endif /* EC16INCLUDES_EC16PRIVATE_EC16UTILITIES_PRIVATE_H_ */
