/*
  Demonstration application for iSYSTEM Training BSC0002
  Written for SAM 3X8E fitted to Arduino DUE board.
 */
#include "Arduino.h"
#include "trafficlight.h"

TrafficLight myTrafficLight;

// the setup function runs once when you press reset or power the board
void setup() {
    // initialize digital pin 13 as an output.
    pinMode(13, OUTPUT);
    
    // Init the traffic light
    trafficLightReset();
    myTrafficLight = getTrafficLight();
}

// the loop function runs over and over again forever
void loop() {

    digitalWrite(13, HIGH);   // turn the LED on (HIGH is the voltage level)
    delay(250);        // wait for a second
    digitalWrite(13, LOW);    // turn the LED off by making the voltage LOW
    delay(250);        // wait for a second

    trafficLightNextState();
    
    myTrafficLight = getTrafficLight();
    
}