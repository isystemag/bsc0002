/*
 * ec16filter_private.h
 *
 *  Created on: 21.03.2017
 *      Author: KREUTZUL
 */

#ifndef EC16INCLUDES_EC16PRIVATE_EC16FILTER_PRIVATE_H_
#define EC16INCLUDES_EC16PRIVATE_EC16FILTER_PRIVATE_H_

#include "ec16private/ec16utilities_private.h"

#define ECLIB_U8_ONE  (u8)1

#endif /* EC16INCLUDES_EC16PRIVATE_EC16FILTER_PRIVATE_H_ */
