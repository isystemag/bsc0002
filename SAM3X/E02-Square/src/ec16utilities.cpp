/*
 * ec16utilities.c
 *
 *  Created on: 20.04.2016
 *      Author: KREUTZUL
 */

#include "ec16private/ec16utilities_private.h"
#include "ec16utilities.h"

s16 ECLIB_s16_ShiftLimitTos16_s32_16(s32 par, s8 sf) {
	s16 tmp_s16;
	s32 tmp_s32;

	tmp_s32 = ECLIB_s32_Shift_s32_16(par, sf);
	tmp_s16 = ECLIB_s16_LimitTos16_s32_16(tmp_s32);

	if ((par < 0) && (tmp_s16 == ECLIB_S16_POS_INF )) {
		tmp_s16 = ECLIB_S16_NEG_INF;
	}

	return tmp_s16;
}

s16 ECLIB_s16_ShiftLimitTos16_u32_16(u32 par, s8 sf) {
	s16 tmp_s16;
	s32 tmp_s32;

	tmp_s32 = ECLIB_s32_LimitTos32_u32_16(par);
	tmp_s32 = ECLIB_s32_Shift_s32_16(tmp_s32, sf);
	tmp_s16 = ECLIB_s16_LimitTos16_s32_16(tmp_s32);

	if (tmp_s16 == ECLIB_S16_POS_INF) {
		tmp_s16 = ECLIB_S16_NEG_INF;
	}

	return tmp_s16;
}

static s32 ECLIB_s32_Eclib_ShiftLimitTos32_s32_16(s32 par, u8 sf, ECLIB_Bool shift_left_flag) {
	u32 par_u32, mask;
	s32 tmp_s32;

	if (par > 0) {
		par_u32 = (u32) par;
	} else {
		par_u32 = (u32) (-par);
	}

	if (sf >= (u8) 32) {
		if (shift_left_flag == ECLIB_TRUE) {
			par_u32 = ECLIB_S32_POS_INF;
		} else {
			par_u32 = 0;
		}
	} else if (shift_left_flag == ECLIB_TRUE) {
		mask = (u32)(((u32)1U << ((u32)31U - (u32)sf)) - (u32)1U);
		if ((par_u32 & (~mask)) != (u32)0) {
			par_u32 = ECLIB_S32_POS_INF;
		} else {
			par_u32 = (par_u32 << (u32) sf);
		}
	} else {
		par_u32 = (par_u32 >> (u32) sf);
	}

	if (par < 0) {
		tmp_s32 = -ECLIB_s32_LimitTos32_u32_16(par_u32);
	} else {
		tmp_s32 = ECLIB_s32_LimitTos32_u32_16(par_u32);
	}
	return tmp_s32;
}

s32 ECLIB_s32_Shift_s32_16(s32 par, s8 sf) {
	s32 tmp_s32;
	u8 sf_u8;
	ECLIB_Bool shift_left_flag;

	if ((par == ECLIB_S32_NAN ) || (par == ECLIB_S32_POS_INF )
			|| (par == ECLIB_S32_NEG_INF )) { // no shift of INFs and NANs
		tmp_s32 = par;
	} else if (sf == 0) {
		tmp_s32 = par;
	} else if (par == 0) {
		tmp_s32 = 0;
	}

	else if (sf > 0) {
		sf_u8 = (u8) sf;
		shift_left_flag = ECLIB_FALSE;	// right shift
		tmp_s32 = ECLIB_s32_Eclib_ShiftLimitTos32_s32_16(par, sf_u8, shift_left_flag);
	} else {
		sf_u8 = (u8) (-sf);
		shift_left_flag = ECLIB_TRUE;	// left shift
		tmp_s32 = ECLIB_s32_Eclib_ShiftLimitTos32_s32_16(par, sf_u8, shift_left_flag);
	}

	return tmp_s32;

}

s16 ECLIB_s16_Shift_s16_16(s16 par, s8 sf) {
	s16 tmp_s16;
	s32 tmp_s32;

	if ((par == ECLIB_S16_NAN ) || (par == ECLIB_S16_POS_INF )
			|| (par == ECLIB_S16_NEG_INF )) {	// no shift of INFs and NANs
		tmp_s16 = par;
	} else {
		tmp_s32 = ECLIB_s32_Shift_s16_16(par, sf);
		tmp_s16 = ECLIB_s16_LimitTos16_s32_16(tmp_s32);
	}

	return tmp_s16;
}

s32 ECLIB_s32_Shift_s16_16(s16 par, s8 sf) {
	s32 tmp_s32;

	if ((par == ECLIB_S16_NAN ) || (par == ECLIB_S16_POS_INF )
			|| (par == ECLIB_S16_NEG_INF )) {	// no shift of INFs and NANs
		tmp_s32 = par;
	} else {

		tmp_s32 = ECLIB_s32_Shift_s32_16((s32) par, sf);
	}
	return tmp_s32;
}

s32 ECLIB_s32_LimitTos32_u32_16(u32 par) {
	s32 ret;
	if (par > (u32) ECLIB_S32_POS_INF) {
		ret = ECLIB_S32_POS_INF;
	} else {
		ret = (s32) par;
	}
	return ret;
}

s16 ECLIB_s16_LimitTos16_s32_16(s32 par) {
	s16 ret_s16;

	if (par == ECLIB_S32_NAN) {
		ret_s16 = ECLIB_S16_NAN;
	}

	else {
		if (par >= (s32) ECLIB_S16_POS_INF) {// cut to S16_POS_INF if overflow
			ret_s16 = ECLIB_S16_POS_INF;
		} else if (par <= ECLIB_S16_NEG_INF) {// cut to S16_NEG_INF if underflow
			ret_s16 = ECLIB_S16_NEG_INF;
		} else {

			// convert to s16
			ret_s16 = (s16) par;
		}
	}
	return ret_s16;
}

s8 ECLIB_s8_LimitTos8_s32_16(s32 par) {
	s8 ret_s8;
	if (par >= (s32) ECLIB_S8_MAX) {				// cut to S8_MAX if overflow
		ret_s8 = ECLIB_S8_MAX;
	} else if (par <= (s32) ECLIB_S8_MIN) {		// cut to S8_MIN if underflow
		ret_s8 = ECLIB_S8_MIN;
	}
	// convert to s8
	else {
		ret_s8 = (s8) par;
	}
	return ret_s8;
}

s32 ECLIB_s32_Abs_s32_16(s32 par) {
	s32 ret_s32;
	if (par > 0) {
		ret_s32 = par;
	} else {
		ret_s32 = (s32) (-par);
	}
	return ret_s32;
}

