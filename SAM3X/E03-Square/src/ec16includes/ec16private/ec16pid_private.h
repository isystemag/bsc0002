/*
 * ec16pid_private.h
 *
 *  Created on: 21.03.2017
 *      Author: KREUTZUL
 */

#ifndef EC16INCLUDES_EC16PRIVATE_EC16PID_PRIVATE_H_
#define EC16INCLUDES_EC16PRIVATE_EC16PID_PRIVATE_H_

#include "ec16utilities_private.h"

#define ECLIB_S32_MAX_I_TERM (ECLIB_S32_POS_INF / 2)

void ECLIB_ResetPIDIntegral_16(ECLIB_PIDType *pid);

#endif /* EC16INCLUDES_EC16PRIVATE_EC16PID_PRIVATE_H_ */
