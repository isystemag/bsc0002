/*
 * ec16filter.h
 *
 *  Created on: 19.09.2016
 *      Author: KREUTZUL
 */

#ifndef EC16INCLUDES_EC16PRIVATE_EC16FILTER_H_
#define EC16INCLUDES_EC16PRIVATE_EC16FILTER_H_


#include "ec16utilities.h"
#include "ec16basicoperation.h"



/*
 *
 * Moving Average
 *
 */


typedef struct {
	s8 value_sf;
	u8 buf_size;
	u8 pos;
	s16* circ_buffer_values;
	s32 sum;
} ECLIB_MovAvgType;


/*
 * Macros for single functions
 */

#define ECLIB_INIT_CIRC_BUF_16(mov_avg_data, val) \
		ECLIB_InitCircBuf_16(&mov_avg_data, ECLIB_pass_fix16(val))

#define ECLIB_INIT_MOV_AVG_16(mov_avg_data, circular_buffer_values, shift_fac, buf_size) \
		ECLIB_InitMovAvg_16(&mov_avg_data, circular_buffer_values, shift_fac, buf_size)

#define ECLIB_SIMPLE_MOV_AVG_16(res, mov_avg_data, cur_in ) \
		ECLIB_SimpleMovAvg_16(ECLIB_pass_fix16(&res), &mov_avg_data, ECLIB_pass_fix16(cur_in))

#define ECLIB_SIMPLE_MOV_AVG_SAFE_16(res, mov_avg_data, cur_in ) \
		ECLIB_SimpleMovAvgSafe_16(ECLIB_pass_fix16(&res), &mov_avg_data, ECLIB_pass_fix16(cur_in))

#define ECLIB_LIN_WEIGHTED_MOV_AVG_16(res, mov_avg_data, cur_in ) \
		ECLIB_LinWeightedMovAvg_16(ECLIB_pass_fix16(&res), &mov_avg_data, ECLIB_pass_fix16(cur_in))


void ECLIB_InitCircBuf_16(ECLIB_MovAvgType *mov_avg_data, ECLIB_rcv_fix16(val));
void ECLIB_InitMovAvg_16(ECLIB_MovAvgType *mov_avg_data, s16 circular_buffer_values[], s8 shift_fac, u8 buf_size);

void ECLIB_SimpleMovAvg_16(ECLIB_rcv_fix16(*res), ECLIB_MovAvgType *mov_avg_data, ECLIB_rcv_fix16(cur_in));
void ECLIB_SimpleMovAvgSafe_16(ECLIB_rcv_fix16(*res), ECLIB_MovAvgType *mov_avg_data, ECLIB_rcv_fix16(cur_in));
void ECLIB_LinWeightedMovAvg_16(ECLIB_rcv_fix16(*res), ECLIB_MovAvgType *mov_avg_data, ECLIB_rcv_fix16(cur_in));


/*
 *
 * Butterworth filter
 *
 */

typedef struct {
	ECLIB_struct_fix16(gain);
	ECLIB_struct_fix16(constant);
	s16 prev_in;
	s16 prev_out;
} ECLIB_FilterType;


/*
 * Macros for single functions
 */

#define ECLIB_INIT_BUTWO_16(filter_data, gain, constant) \
		ECLIB_InitButwo_16(&filter_data, ECLIB_pass_fix16(gain), ECLIB_pass_fix16(constant))

#define ECLIB_BUTWO_LP_FILTER_16(res, filter_data, cur_in) \
		ECLIB_ButwoLPFilter_16(ECLIB_pass_fix16(&res), &filter_data, ECLIB_pass_fix16(cur_in))

#define ECLIB_BUTWO_HP_FILTER_16(res, filter_data, cur_in) \
		ECLIB_ButwoHPFilter_16(ECLIB_pass_fix16(&res), &filter_data, ECLIB_pass_fix16(cur_in))


void ECLIB_InitButwo_16(ECLIB_FilterType *filter_data, ECLIB_rcv_fix16(gain), ECLIB_rcv_fix16(constant)) ;
void ECLIB_ButwoLPFilter_16(ECLIB_rcv_fix16(*res), ECLIB_FilterType *filter_data, ECLIB_rcv_fix16(cur_in));
void ECLIB_ButwoHPFilter_16(ECLIB_rcv_fix16(*res), ECLIB_FilterType *filter_data, ECLIB_rcv_fix16(cur_in));



#endif /* EC16INCLUDES_EC16PRIVATE_EC16FILTER_H_ */
