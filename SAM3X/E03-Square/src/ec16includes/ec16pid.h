/*
 * ec16pid.h
 *
 *  Created on: 07.10.2016
 *      Author: KREUTZUL
 */

#ifndef EC16INCLUDES_EC16PRIVATE_EC16PID_H_
#define EC16INCLUDES_EC16PRIVATE_EC16PID_H_


#include "ec16utilities.h"
#include "ec16basicoperation.h"

typedef struct {
	s8 sf;				// shift factor

	// start values for PID controller (should be initialized with 0)
	s16 oldValue;		// last process value
	s32 sumError;		// sum of all errors (integral)

	// gains
	s16 pFactor;		// gain proportional term
	s16 iFactor;		// gain integral term        	(i_Factor = 0 => PD controller)
	s16 dFactor;		// gain derivative term			(d_Factor = 0 => PI controller)

	// limits to obviate overflow
	s32 maxSumError;	// max error for I-Term
} ECLIB_PIDType;






/*
 *
 * Macros for single functions
 *
 */

#define ECLIB_INIT_PI_16(p_factor, i_factor, pid_sf, pid) \
		ECLIB_InitPID_16(p_factor, pid_sf, i_factor, pid_sf, 0, pid_sf, &pid)

#define ECLIB_INIT_PD_16(p_factor, d_factor, pid_sf, pid) \
		ECLIB_InitPID_16(p_factor, pid_sf, 0, pid_sf, d_factor, pid_sf, &pid)

#define ECLIB_INIT_PID_16(p_factor, i_factor, d_factor, pid_sf, pid) \
		ECLIB_InitPID_16(p_factor, pid_sf, i_factor, pid_sf, d_factor, pid_sf, &pid)

#define ECLIB_CONT_PID_16(res, setPoint, actValue, pid) \
		ECLIB_ContPID_16(ECLIB_pass_fix16(&res), ECLIB_pass_fix16(setPoint), ECLIB_pass_fix16(actValue), &pid)

#define ECLIB_RESET_PID_OLD_VALUE_16(pid, val) \
		ECLIB_ResetPIDOldValue_16(&pid, val);

void ECLIB_InitPID_16(ECLIB_rcv_fix16(p_factor), ECLIB_rcv_fix16(i_factor), ECLIB_rcv_fix16(d_factor), ECLIB_PIDType *pid);
void ECLIB_ContPID_16(ECLIB_rcv_fix16(*res), ECLIB_rcv_fix16(setPoint), ECLIB_rcv_fix16(actValue), ECLIB_PIDType *pid) ;

void ECLIB_ResetPIDOldValue_16(ECLIB_PIDType *pid, s16 val);


#endif /* EC16INCLUDES_EC16PRIVATE_EC16PID_H_ */
