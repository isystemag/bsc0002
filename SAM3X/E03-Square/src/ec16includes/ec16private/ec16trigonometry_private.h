/*
 * ec16trigonometry_private.h
 *
 *  Created on: 21.03.2017
 *      Author: KREUTZUL
 */




#ifndef EC16INCLUDES_EC16PRIVATE_EC16TRIGONOMETRY_PRIVATE_H_
#define EC16INCLUDES_EC16PRIVATE_EC16TRIGONOMETRY_PRIVATE_H_


#include "ec16private/ec16utilities_private.h"

#define ECLIB_INTERN_360_DEG	   	(s16)0x8000		//-32768
#define ECLIB_INTERN_270_DEG	   	(s16)0x6000		// 24576
#define ECLIB_INTERN_180_DEG	   	(s16)0x4000		// 16384
#define ECLIB_INTERN_90_DEG			(s16)0x2000		//  8192



/*
 * n : Q-pos for quarter circle				13
 * A : Q-pos for output						12	(return sine value Q12)
 * p : Q-pos for parentheses intermediate	15
 * r = 2n-p									11
 * s = -(A-1-p-n)							17
 */
#define ECLIB_qN (s8)13
#define ECLIB_qA (s8)12
#define ECLIB_qP (s8)15

#endif /* EC16INCLUDES_EC16PRIVATE_EC16TRIGONOMETRY_PRIVATE_H_ */
