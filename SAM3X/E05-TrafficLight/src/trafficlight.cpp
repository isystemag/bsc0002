/*******************************************************************************
 *
 *
 *
 ******************************************************************************/

#include "trafficlight.h"
 
enum TrafficLightState {
    STATUS_INIT,
    STATUS_RED,
    STATUS_RED_AMBER,
    STATUS_GREEN,
    STATUS_AMBER,
    STATUS_ERROR_1,
    STATUS_ERROR_2
};
 
static TrafficLightState _trafficLightState = STATUS_INIT;
static TrafficLightState _trafficLightNextState = STATUS_INIT;
static TrafficLight _trafficLight = LIGHT_ALL_OFF;

void __attribute__ ((noinline)) trafficLightReset(void) {
    // Resetting state machine puts "intialise" into
    // our two-stage pipeline
    _trafficLightState = STATUS_INIT;
    _trafficLightNextState = STATUS_INIT;
} 

void __attribute__ ((noinline)) trafficLightError(void) {
    // In the event of an error, start error status
    // sequence
    _trafficLightNextState = STATUS_ERROR_1;
    trafficLightNextState();
}

void __attribute__ ((noinline)) trafficLightNextState(void) {
    _trafficLightState = _trafficLightNextState;
    
    // Move through the state machine, setting up next
    // light in the sequence.
    // Exception are the STATUS_ERROR_x states that drop
    // into a state that results in the amber light flashing
    // on and off
    switch (_trafficLightState) {
        case STATUS_INIT:
            _trafficLightNextState = STATUS_AMBER;
            break;
            
        case STATUS_RED:
            _trafficLightNextState = STATUS_RED_AMBER;
            break;
            
        case STATUS_RED_AMBER:
            _trafficLightNextState = STATUS_GREEN;
            break;
            
        case STATUS_GREEN:
            _trafficLightNextState = STATUS_AMBER;
            break;
            
        case STATUS_AMBER:
            _trafficLightNextState = STATUS_RED;
            break;
            
        case STATUS_ERROR_1:
            _trafficLightNextState = STATUS_ERROR_2;
            break;
        
        case STATUS_ERROR_2:
            _trafficLightNextState = STATUS_ERROR_1;
            break;
            
        default:
            _trafficLightState = STATUS_ERROR_1;
            _trafficLightNextState = STATUS_ERROR_2;
    }
}

TrafficLight __attribute__ ((noinline)) getTrafficLight(void) {
    TrafficLight returnValue = LIGHT_ALL_OFF;
    
    switch (_trafficLightState) {
        case STATUS_INIT:
            /* Do nothing - return default lights off state */
            break;
            
        case STATUS_RED:
            returnValue = LIGHT_RED;
            break;
        
        case STATUS_RED_AMBER:
            returnValue = LIGHT_RED_AMBER;
            break;
            
        case STATUS_GREEN:
            returnValue = LIGHT_GREEN;
            break;
        
        case STATUS_AMBER:
            returnValue = LIGHT_AMBER;
            break;
            
        case STATUS_ERROR_1:
            returnValue = LIGHT_ALL_OFF;
        
        case STATUS_ERROR_2:
            returnValue = LIGHT_AMBER;
        
        default:
            returnValue = LIGHT_ERROR;
    }
    
    return returnValue;
}