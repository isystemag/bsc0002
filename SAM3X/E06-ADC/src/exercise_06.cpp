/*
  Demonstration application for iSYSTEM Training BSC0002
  Written for SAM 3X8E fitted to Arduino DUE board.
 */
#include "Arduino.h"

unsigned int getADC(void);
boolean setDelay(void);

// the setup function runs once when you press reset or power the board
void setup() {
    // initialize digital pin 13 as an output.
    pinMode(13, OUTPUT);
}

// the loop function runs over and over again forever
void loop() {
    unsigned int waitTime = 0;
        
    if (setDelay()) {
        waitTime = 100;
    } else {
        waitTime = 500;
    }

    digitalWrite(13, HIGH);   // turn the LED on (HIGH is the voltage level)
    delay(waitTime);        // wait for a second
    digitalWrite(13, LOW);    // turn the LED off by making the voltage LOW
    delay(waitTime);        // wait for a second

}

unsigned int __attribute__ ((noinline)) getADC(void) {
    unsigned int returnValue = 0;
    
    returnValue = analogRead(3);
    
    return returnValue;
}

boolean __attribute__ ((noinline)) setDelay(void) {
    boolean returnValue = true;
    unsigned int adcValue = 0;
    
    adcValue = getADC();
    
    if (adcValue >= 512) {
        returnValue = false;
    }
    
    return returnValue;
}
