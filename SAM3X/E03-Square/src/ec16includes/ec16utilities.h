/*
 * ec16utilities.h
 *
 *  Created on: 20.04.2016
 *      Author: KREUTZUL
 */

#ifndef EC16FIXEDPOINT_EC16UTILITIES_H_
#define EC16FIXEDPOINT_EC16UTILITIES_H_



#include "ec16datatypes.h"
#include "ec16defines.h"


/*
 *
 * Macros for calling
 *
 */



#define ECLIB_pass_fix16(par) \
	par, \
	par##_sf

#define ECLIB_rcv_fix16(par) \
	s16 par, \
	const s8 par##_sf


#define ECLIB_SET_FIX16_16(par, val) {	\
	s8 par_sf = par##_sf; \
	if (val != val){ \
		par = ECLIB_S16_NAN; \
	} \
	else {\
		if (par_sf >= 0) { \
			if (((val) * (float) (1 << par_sf)) > 32767.0F) { 	/* cut to S16_POS_INF if overflow */ \
				par = ECLIB_S16_POS_INF;	\
			}	\
			else if(((val) * (float) (1 << par_sf)) < -32768.0F) { 	/* cut to S16_NEG_INF if underflow */	\
				par = ECLIB_S16_NEG_INF;	\
			}	\
			else {	/* convert to s16 */ \
				par = (s16) ((val) * (float) (1 << par_sf));	\
			}	\
		} \
		else { /* par_sf < 0) */ \
			if (((val) / (float) (1 << (-par_sf))) > 32767.0F) { 	/* cut to S16_POS_INF if overflow */ \
				par = ECLIB_S16_POS_INF;	\
			}	\
			else if(((val) / (float) (1 << (-par_sf))) < -32768.0F) { 	/* cut to S16_NEG_INF if underflow */	\
				par = ECLIB_S16_NEG_INF;	\
			}	\
			else {	/* convert to s16 */ \
				par = (s16) ((val) / (float) (1 << (-par_sf)));	\
			}	\
		} \
	}\
}



#endif /* EC16FIXEDPOINT_EC16UTILITIES_H_ */
