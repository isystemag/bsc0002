/*
  Demonstration application for iSYSTEM Training BSC0002
  Written for SAM 3X8E fitted to Arduino DUE board.
 */
#include "Arduino.h"

enum class tTypes {
    tC,
    tF,
    tK
};

class Temperature {
    private:
        float _tCelcius;
        float _tFahrenheit;
        float _tKelvin;
        float convCtoF(float temperature);
        float convCtoK(float temperature);
        float convFtoC(float temperature);
        float convFtoK(float temperature);
        float convKtoC(float temperature);
        float convKtoF(float temperature);
        
    public:
        float getTemperature();
        float getTemperature(tTypes type);
        void setTemperature(float temperature);
        void setTemperature(float temperature, tTypes type);
        Temperature();
        Temperature(float temperature);
        Temperature(float temperature, tTypes type);
};

Temperature gndFloor;
Temperature firstFloor(35.0);
Temperature secondFloor(85.0, tTypes::tK);

float tKelvin;
float tCelcius;
float tFahrenheit;

// the setup function runs once when you press reset or power the board
void setup() {
    // initialize digital pin 13 as an output.
    pinMode(13, OUTPUT);
}

// the loop function runs over and over again forever
void loop() {

    digitalWrite(13, HIGH);   // turn the LED on (HIGH is the voltage level)
    delay(250);        // wait for a second
    digitalWrite(13, LOW);    // turn the LED off by making the voltage LOW
    delay(250);        // wait for a second

    gndFloor.setTemperature(25);
    
    tKelvin = gndFloor.getTemperature(tTypes::tK);
    tFahrenheit = gndFloor.getTemperature(tTypes::tF);
    tCelcius = gndFloor.getTemperature(tTypes::tC);
    
    gndFloor.setTemperature(25, tTypes::tK);
    
    tKelvin = gndFloor.getTemperature(tTypes::tK);
    tFahrenheit = gndFloor.getTemperature(tTypes::tF);
    tCelcius = gndFloor.getTemperature();
}

Temperature::Temperature(void) {
    _tCelcius = 0.0;
    _tFahrenheit = convCtoF(_tCelcius);
    _tKelvin = convCtoK(_tCelcius);
}

Temperature::Temperature(float temperature) {
    _tCelcius = temperature;
    _tFahrenheit = convCtoF(temperature);
    _tKelvin = convCtoK(temperature);
}

Temperature::Temperature(float temperature, tTypes type) {
    if (type == tTypes::tC) {
        Temperature(_tCelcius);
    } else if (type == tTypes::tF) {
        _tFahrenheit = temperature;
        _tCelcius = convFtoC(_tFahrenheit);
        _tKelvin = convFtoK(_tFahrenheit);
    } else {
        /* Assume Kelvin value */
        _tKelvin = temperature;
        _tCelcius = convKtoC(_tKelvin);
        _tFahrenheit = convKtoF(_tKelvin);
    }
}

float Temperature::convCtoF(float temperature) {
    return ((temperature * (9.0/5.0)) + 32.0);
}

float Temperature::convCtoK(float temperature) {
    return (temperature + 273.15);
}

float Temperature::convKtoF(float temperature) {
    return ((temperature * (9.0/5.0)) - 459.67);
}

float Temperature::convKtoC(float temperature) {
    return (temperature - 273.15);
}

float Temperature::convFtoC(float temperature) {
    return ((temperature - 32.0) * (5.0/9.0));
}

float Temperature::convFtoK(float temperature) {
    return ((temperature + 459.67) * (5.0/9.0));
}

void Temperature::setTemperature(float temperature) {
    _tCelcius = temperature;
    _tFahrenheit = convCtoF(temperature);
    _tKelvin = convCtoK(temperature);
}

void Temperature::setTemperature(float temperature, tTypes type) {
    if (type == tTypes::tC) {
        Temperature(_tCelcius);
    } else if (type == tTypes::tF) {
        _tFahrenheit = temperature;
        _tCelcius = convFtoC(_tFahrenheit);
        _tKelvin = convFtoK(_tFahrenheit);
    } else {
        /* Assume Kelvin value */
        _tKelvin = temperature;
        _tCelcius = convFtoC(_tKelvin);
        _tFahrenheit = convKtoF(_tKelvin);
    }
}

float Temperature::getTemperature() {
    return _tCelcius;
}

float Temperature::getTemperature(tTypes type) {
    float returnValue = 0.0;
    
    if (type == tTypes::tC) {
        returnValue = _tCelcius;
    } else if (type == tTypes::tF) {
        returnValue = _tFahrenheit;
    } else {
        /* Assume Kelvin value */
        returnValue = _tKelvin;
    }
    return returnValue;
}