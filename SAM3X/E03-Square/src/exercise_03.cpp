/*
  Demonstration application for iSYSTEM Training BSC0002
  Written for SAM 3X8E fitted to Arduino DUE board.
  Makes use of EC-LIB,an Eclipseina product (www.eclipseina.com)
  For more information, visit http://www.eclipseina.com/index.php/ec-lib.html
 */
#include "Arduino.h"

#include "./ec16includes/ec16power.h"

ECLIB_fix16_0sr(result);
ECLIB_fix16_0sr(parameter);

// the setup function runs once when you press reset or power the board
void setup() {
    // initialize digital pin 13 as an output.
    pinMode(13, OUTPUT);

    parameter = 4;

    result = 0;
}

// the loop function runs over and over again forever
void loop() {
    digitalWrite(13, HIGH);
    delay(750);
    
    ECLIB_Sqr_16(ECLIB_pass_fix16(&result), ECLIB_pass_fix16(parameter));
    
    digitalWrite(13, LOW);
    delay(750);
}

