# README #

Code Repository for the On-Line Course BSC0002 - Introduction to testIDEA

## What is this repository for? ##

This respository contains all of the code required to accompany the laboratory excercises for the online course BSC0002 - Introduction to testIDEA.
The source code for various microcontrollers is included, but it is not necessary to build any application to use this code. All projects have
a pre-compiled ELF file for direct use within testIDEA.

## How do I get set up? ##

Simply download this repository in its entirity to your development PC (use the 'Downloads' link here on the left-hand side) and un-zip to a suitable
location on your hard drive.

If you wish, you can also use git to clone the respository.

## Support ##

Currently this respository supports the following BlueBox On-Chip Analyzers:

* iC5000
* (In Development) iC5700

The following microcontrollers and development boards are also covered in the project:

* Microchip (Atmel) SAM3X8E (Cortex-M3) together with the Arduino DUE board
* (In Development) Renesas R7F701057 (RH850) together with the RH850-F1x Starter-Kit F1L V2
* (In Development) Infineon AURIX
* (In Development) NXP MPC 55/56/57

## Who do I talk to? ##

If you require support, please contact stuart.cording@isystem.com directly.


