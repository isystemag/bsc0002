/*
 * ec16basicoperation.h
 *
 *  Created on: 15.04.2016
 *      Author: KREUTZUL
 */

#ifndef SRC_EC16BASICOPERATIONS_H_
#define SRC_EC16BASICOPERATIONS_H_



#include "ec16utilities.h"





/*
 *
 * Macros for single functions
 *
 */

#define ECLIB_ADD_16(res, par1, par2) \
		ECLIB_Add_16( ECLIB_pass_fix16(&res), ECLIB_pass_fix16(par1), ECLIB_pass_fix16(par2) )

#define ECLIB_SUB_16(res, par1, par2) \
		ECLIB_Sub_16( ECLIB_pass_fix16(&res), ECLIB_pass_fix16(par1), ECLIB_pass_fix16(par2) )

#define ECLIB_MUL_16(res, par1, par2) \
		ECLIB_Mul_16( ECLIB_pass_fix16(&res), ECLIB_pass_fix16(par1), ECLIB_pass_fix16(par2) )

#define ECLIB_DIV_16(res, par1, par2) \
		ECLIB_Div_16( ECLIB_pass_fix16(&res), ECLIB_pass_fix16(par1), ECLIB_pass_fix16(par2) )


void ECLIB_Add_16(ECLIB_rcv_fix16(*res), ECLIB_rcv_fix16(par1), ECLIB_rcv_fix16(par2));
void ECLIB_Sub_16(ECLIB_rcv_fix16(*res), ECLIB_rcv_fix16(par1), ECLIB_rcv_fix16(par2));
void ECLIB_Mul_16(ECLIB_rcv_fix16(*res), ECLIB_rcv_fix16(par1), ECLIB_rcv_fix16(par2));
void ECLIB_Div_16(ECLIB_rcv_fix16(*res), ECLIB_rcv_fix16(par1), ECLIB_rcv_fix16(par2));



#define ECLIB_ASSIGN_16(res, par) \
		ECLIB_Assign_16(ECLIB_pass_fix16(&res), ECLIB_pass_fix16(par) )

#define ECLIB_ABS_16(res, par) \
		ECLIB_Abs_16(ECLIB_pass_fix16(&res), ECLIB_pass_fix16(par) )


void ECLIB_Assign_16(ECLIB_rcv_fix16(*res), ECLIB_rcv_fix16(par));
void ECLIB_Abs_16(ECLIB_rcv_fix16(*res), ECLIB_rcv_fix16(par));



#define ECLIB_GET_MAX_OF_16(res, par1, par2) \
		ECLIB_GetMaxOf_16(ECLIB_pass_fix16(&res), ECLIB_pass_fix16(par1), ECLIB_pass_fix16(par2))

#define ECLIB_GET_MIN_OF_16(res, par1, par2) \
		ECLIB_GetMinOf_16(ECLIB_pass_fix16(&res), ECLIB_pass_fix16(par1), ECLIB_pass_fix16(par2))

void ECLIB_GetMaxOf_16(ECLIB_rcv_fix16(*res), ECLIB_rcv_fix16(par1), ECLIB_rcv_fix16(par2));
void ECLIB_GetMinOf_16(ECLIB_rcv_fix16(*res), ECLIB_rcv_fix16(par1), ECLIB_rcv_fix16(par2));
#endif /* SRC_EC16BASICOPERATIONS_H_ */
