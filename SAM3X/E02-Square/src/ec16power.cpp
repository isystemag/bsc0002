/*
 * ec16power.c
 *
 *  Created on: 21.06.2016
 *      Author: KREUTZUL
 */



#include "ec16private/ec16power_private.h"
#include "ec16power.h"

/*
 *
 * 	par^2
 *
 *
 */

static s32 s32_Eclib_Square_s16(s16 par) {
	s32 res_32;

	res_32 = (s32) par * (s32) par;

	return res_32;
}

void ECLIB_Sqr_16(ECLIB_rcv_fix16(*res), ECLIB_rcv_fix16(par)) {

	s32 res_32;
	if (par == ECLIB_S16_NAN) {
		*res = ECLIB_S16_NAN;
	} else if (par == 0) {
		*res = 0;
	}

	else if (ECLIB_bool_IsInfinity_s16_16(par) == ECLIB_TRUE) {
		*res = ECLIB_S16_POS_INF;
	} else {
		res_32 = s32_Eclib_Square_s16(par);								// a*a
		*res = ECLIB_s16_ShiftLimitTos16_s32_16(res_32,
				ECLIB_s8_LimitTos8_s32_16(-((s32)*res_sf - (2 * (s32)par_sf))));	// a << (sf_res - 2*sf_a)
	}
}

/*
 *
 * 	par^exp
 *
 *  p.e.:   a^23 = a^1 * a^2 * a^4 * a^16 *
 *  algorithm: exponent / 2, if exponent is even don't multiply with matching power of a
 *
 *  23   | a^1
 *  11   | a^2
 *   5   | a^4
 *   2   | -----     ( 2   | a^8 )
 *   1   | a^16
 *
 */

static s32 s32_Eclib_Power_s16_s16(s16 par, s16 exp) {
	s32 res_32 = 1;
	s32 tmp = (s32) par;

	while ((exp > 0) && (res_32 < ECLIB_S32_POS_INF ) && (res_32 > ECLIB_S32_NEG_INF )) {
		if (ECLIB_bool_IsEven_s16_16(exp) == ECLIB_FALSE) { /**/
			if ((tmp >= ECLIB_S16_POS_INF ) && (res_32 >= (ECLIB_S32_POS_INF / tmp))) {
				res_32 = ECLIB_S32_POS_INF;
			} else if ((tmp >= ECLIB_S16_POS_INF )
					&& (res_32 <= (ECLIB_S32_NEG_INF / tmp))) {
				res_32 = ECLIB_S32_NEG_INF;
			} else if ((res_32 >= ECLIB_S16_POS_INF )
					&& (tmp >= (ECLIB_S32_POS_INF / res_32))) {
				res_32 = ECLIB_S32_POS_INF;
			} else if ((res_32 >= ECLIB_S16_POS_INF )
					&& (tmp <= (ECLIB_S32_NEG_INF / res_32))) {
				res_32 = ECLIB_S32_NEG_INF;
			} else if ((res_32 <= ECLIB_S16_NEG_INF )
					&& (tmp >= (ECLIB_S32_NEG_INF / res_32))) {
				res_32 = ECLIB_S32_NEG_INF;
			} else if ((res_32 <= ECLIB_S16_NEG_INF )
					&& (tmp <= (ECLIB_S32_POS_INF / res_32))) {
				res_32 = ECLIB_S32_POS_INF;
			} else {
				res_32 = res_32 * tmp; /**/
			}
		}

		if ((tmp > ECLIB_S16_POS_INF ) || (tmp < ECLIB_S16_NEG_INF )) {
			tmp = ECLIB_S32_POS_INF;
		} else {
			tmp = tmp * tmp; /**/
		}

		exp = (s16) (exp / 2); /**/
	}
	return (res_32);
}

/*
 * exponent is not used like a fix16 yet, only like an int16.
 */
void ECLIB_Pow_16(ECLIB_rcv_fix16(*res), ECLIB_rcv_fix16(par), ECLIB_rcv_fix16(exp)) {
	s8 temp_sf_8;
	s32 help_32, temp_sf_32;
	s32 res_32;
	s16 shifted_par = ECLIB_s16_Shift_s16_16(par, par_sf);
	s16 shifted_one = ECLIB_s16_Shift_s16_16(1, -par_sf);

	ECLIB_ASSERT(exp_sf == 0, "ECLIB_Pow_16: exponent should have shift factor 0");

	if ((par == ECLIB_S16_NAN ) || (exp == ECLIB_S16_NAN )) {
		*res = ECLIB_S16_NAN;
	}

	else if (exp == 0) {
		if (*res_sf < 0) {
			*res = 0;
		} else if (*res_sf >= 16) {
			*res = ECLIB_S16_POS_INF;
		} else {
			*res = ECLIB_s16_Shift_s16_16(1, ((-1) * *res_sf));
		}
	}

	else if (par == 0) {
		*res = 0;
	}

	else if (par == ECLIB_S16_POS_INF) {
		if (exp > 0) {
			*res = ECLIB_S16_POS_INF;
		} else {
			*res = 0;
		}
	}

	else if (par == ECLIB_S16_NEG_INF) {
		if (exp == ECLIB_S16_NEG_INF) {
			*res = 0;
		} else if (exp == ECLIB_S16_POS_INF) {
			*res = ECLIB_S16_POS_INF;
		} else if (exp < 0) {
			*res = 0;
		} else if (ECLIB_bool_IsEven_s16_16(exp) == ECLIB_TRUE) {
			*res = ECLIB_S16_POS_INF;
		} else {
			*res = ECLIB_S16_NEG_INF;
		}
	}

	else if (ECLIB_bool_IsInfinity_s16_16(exp) == ECLIB_TRUE) {

		if (-par == shifted_one) { // this means (shifted_par == -1),  but it is exacter
			*res = ECLIB_S16_NAN;
		} else if (par == shifted_one) {		// this means (shifted_par == 1)
			*res = ECLIB_s16_Shift_s16_16(1, (s8) ((-1) * (*res_sf)));
		} else {
			if (exp == ECLIB_S16_POS_INF) {
				if (((ECLIB_s32_Abs_s32_16(par) < shifted_one) && (par_sf > 0))
						|| (par_sf > 32)) {			//  -1 < shifted_par < 1
					*res = 0;
				} else {
					*res = ECLIB_S16_POS_INF;
				}
			}
			if (exp == ECLIB_S16_NEG_INF) {
				if (((ECLIB_s32_Abs_s32_16(par) < shifted_one) && (par_sf > 0))
						|| (par_sf > 32)) {			//  -1 < shifted_par < 1
					*res = ECLIB_S16_POS_INF;
				} else {
					*res = 0;
				}
			}
		}
	}

	  else if (shifted_par == ECLIB_S16_NEG_INF) {
		if (exp < 0) {
			*res = 0;
		} else if (ECLIB_bool_IsEven_s16_16(exp) == ECLIB_TRUE) {
			*res = ECLIB_S16_POS_INF;
		} else {// (bool_Eclib_IsEven_s16(exp) == EC_FALSE) && par == S16_NEG_INF
			*res = ECLIB_S16_NEG_INF;
		}
	}


 else if (shifted_par == ECLIB_S16_POS_INF) {
		if (exp > 0) {
			*res = ECLIB_S16_POS_INF;
		} else {
			*res = 0;
		}
	}

	else if (exp < 0) {
		temp_sf_32 = (s32) *res_sf + ((s32) (-exp) * (s32) par_sf);
		temp_sf_8 = ECLIB_s8_LimitTos8_s32_16(-temp_sf_32);
		help_32 = ECLIB_s32_Shift_s32_16(1, temp_sf_8);

		res_32 = s32_Eclib_Power_s16_s16((s16) (par), (s16) (-exp));

		if ((res_32 == ECLIB_S32_POS_INF ) || (res_32 == ECLIB_S32_NEG_INF )) {
			*res = 0;
		} else if (help_32 == ECLIB_S32_POS_INF) {
			if (res_32 > 0) {
				*res = ECLIB_S16_POS_INF;
			} else {
				*res = ECLIB_S16_NEG_INF;
			}
		} else {
			res_32 = help_32 / res_32;
			*res = ECLIB_s16_LimitTos16_s32_16(res_32);
		}

	} else {
		temp_sf_32 = (s32) *res_sf + ((s32) exp * (s32) (-par_sf));
		temp_sf_8 = ECLIB_s8_LimitTos8_s32_16(temp_sf_32);

		res_32 = s32_Eclib_Power_s16_s16(par, exp);
/*lint -e9007 */
		if ((ECLIB_bool_IsInfinity_s32_16(res_32) == ECLIB_TRUE)
				&& (((ECLIB_s32_Abs_s32_16(par) < shifted_one) && (par_sf > 0))
						|| (par_sf > 32)))
/*lint +e9007 */
		{	//  -1  <  shifted_par < 1
			*res = 0;
		} else {
			*res = ECLIB_s16_ShiftLimitTos16_s32_16(res_32,
					ECLIB_s8_LimitTos8_s32_16(-temp_sf_8));
		}

	}
}
