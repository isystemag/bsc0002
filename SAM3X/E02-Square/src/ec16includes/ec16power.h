/*
 * ec16power.h
 *
 *  Created on: 21.06.2016
 *      Author: KREUTZUL
 */

#ifndef EC16FIXEDPOINT_EC16POWER_H_
#define EC16FIXEDPOINT_EC16POWER_H_


#include "ec16utilities.h"

/*
 *
 * Macros for single functions
 *
 */

#define ECLIB_POW_16(res, par, exp) \
		ECLIB_Pow_16( ECLIB_pass_fix16(&res), ECLIB_pass_fix16(par), ECLIB_pass_fix16(exp) )
#define ECLIB_SQR_16(res, par) \
		ECLIB_Sqr_16( ECLIB_pass_fix16(&res), ECLIB_pass_fix16(par) )


void ECLIB_Pow_16(ECLIB_rcv_fix16(*res), ECLIB_rcv_fix16(par), ECLIB_rcv_fix16(exp));
void ECLIB_Sqr_16(ECLIB_rcv_fix16(*res), ECLIB_rcv_fix16(par));





#endif /* EC16FIXEDPOINT_EC16POWER_H_ */
