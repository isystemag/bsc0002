/*
 * ec16trigonometry.h
 *
 *  Created on: 29.06.2016
 *      Author: KREUTZUL
 */

#ifndef EC16FIXEDPOINT_EC16TRIGONOMETRY_H_
#define EC16FIXEDPOINT_EC16TRIGONOMETRY_H_


#include "ec16basicoperation.h"
#include "ec16compare.h"




#define ECLIB_360_DEG	   	(s16)0x4000	//(s16)(ECLIB_INTERN_360_DEG / 2)
#define ECLIB_270_DEG	   	(s16)0x3000	//(s16)(ECLIB_INTERN_270_DEG / 2)
#define ECLIB_180_DEG	   	(s16)0x2000	//(s16)(ECLIB_INTERN_180_DEG / 2)
#define ECLIB_90_DEG	   	(s16)0x1000	//(s16)(ECLIB_INTERN_90_DEG  / 2)


/*
 *
 * Macros for single functions
 *
 */
#define ECLIB_SIN_16(res, par) \
		ECLIB_Sin_16( ECLIB_pass_fix16(&res), ECLIB_pass_fix16(par) )
#define ECLIB_COS_16(res, par) \
		ECLIB_Cos_16( ECLIB_pass_fix16(&res), ECLIB_pass_fix16(par) )
#define ECLIB_TAN_16(res, par) \
		ECLIB_Tan_16( ECLIB_pass_fix16(&res), ECLIB_pass_fix16(par) )
#define ECLIB_COT_16(res, par) \
		ECLIB_Cot_16( ECLIB_pass_fix16(&res), ECLIB_pass_fix16(par) )


void ECLIB_Sin_16(ECLIB_rcv_fix16(*res), ECLIB_rcv_fix16(par));
void ECLIB_Cos_16(ECLIB_rcv_fix16(*res), ECLIB_rcv_fix16(par));
void ECLIB_Tan_16(ECLIB_rcv_fix16(*res), ECLIB_rcv_fix16(par));
void ECLIB_Cot_16(ECLIB_rcv_fix16(*res), ECLIB_rcv_fix16(par));

#endif /* EC16FIXEDPOINT_EC16TRIGONOMETRY_H_ */
