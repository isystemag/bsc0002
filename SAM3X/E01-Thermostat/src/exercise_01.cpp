/*
  Demonstration application for iSYSTEM Training BSC0002
  Written for SAM 3X8E fitted to Arduino DUE board.
 */
#include "Arduino.h"

enum Thermostat {
    TEMP_ERROR,
    TEMP_UNDER_15,
    TEMP_OK,
    TEMP_OVER_40
};

Thermostat evaluateTemperature(signed int temperature);
Thermostat thermostateControl = TEMP_ERROR;

// the setup function runs once when you press reset or power the board
void setup() {
    // initialize digital pin 13 as an output.
    pinMode(13, OUTPUT);
}

// the loop function runs over and over again forever
void loop() {

    digitalWrite(13, HIGH);   // turn the LED on (HIGH is the voltage level)
    delay(250);        // wait for a second
    digitalWrite(13, LOW);    // turn the LED off by making the voltage LOW
    delay(250);        // wait for a second

    thermostateControl = evaluateTemperature(25);
    
    if (thermostateControl == TEMP_ERROR) {
        digitalWrite(13, HIGH);  
    }
    
    thermostateControl = TEMP_ERROR;
}

Thermostat __attribute__ ((noinline)) evaluateTemperature(signed int temperature) {
    Thermostat returnValue = TEMP_ERROR;
    
    if (temperature < 15) {
        returnValue = TEMP_UNDER_15;
    } else if (temperature <= 40) {
        returnValue = TEMP_OK;
    } else {
        returnValue = TEMP_OVER_40;
    }
    
    return returnValue;
}
