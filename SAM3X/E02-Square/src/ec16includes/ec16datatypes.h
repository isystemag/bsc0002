/*
 * ec16datatypes.h
 *
 *  Created on: 19.04.2016
 *      Author: KREUTZUL
 */

#ifndef SRC_EC16FIXEDPOINT_EC16DATATYPES_H_
#define SRC_EC16FIXEDPOINT_EC16DATATYPES_H_


#include "../common/ecdatatypes.h"



#define ECLIB_fix16_0sr(par) \
	s16 par; \
	const s8 par##_sf = 0

#define ECLIB_fix16_1sr(par) \
	s16 par; \
	const s8 par##_sf = 1
#define ECLIB_fix16_2sr(par) \
	s16 par; \
	const s8 par##_sf = 2
#define ECLIB_fix16_3sr(par) \
	s16 par; \
	const s8 par##_sf = 3
#define ECLIB_fix16_4sr(par) \
	s16 par; \
	const s8 par##_sf = 4
#define ECLIB_fix16_5sr(par) \
	s16 par; \
	const s8 par##_sf = 5
#define ECLIB_fix16_6sr(par) \
	s16 par; \
	const s8 par##_sf = 6
#define ECLIB_fix16_7sr(par) \
	s16 par; \
	const s8 par##_sf = 7
#define ECLIB_fix16_8sr(par) \
	s16 par; \
	const s8 par##_sf = 8
#define ECLIB_fix16_9sr(par) \
	s16 par; \
	const s8 par##_sf = 9
#define ECLIB_fix16_10sr(par) \
	s16 par; \
	const s8 par##_sf = 10
#define ECLIB_fix16_11sr(par) \
	s16 par; \
	const s8 par##_sf = 11
#define ECLIB_fix16_12sr(par) \
	s16 par; \
	const s8 par##_sf = 12
#define ECLIB_fix16_13sr(par) \
	s16 par; \
	const s8 par##_sf = 13
#define ECLIB_fix16_14sr(par) \
	s16 par; \
	const s8 par##_sf = 14
#define ECLIB_fix16_15sr(par) \
	s16 par; \
	const s8 par##_sf = 15
#define ECLIB_fix16_16sr(par) \
	s16 par; \
	const s8 par##_sf = 16
#define ECLIB_fix16_17sr(par) \
	s16 par; \
	const s8 par##_sf = 17
#define ECLIB_fix16_18sr(par) \
	s16 par; \
	const s8 par##_sf = 18
#define ECLIB_fix16_19sr(par) \
	s16 par; \
	const s8 par##_sf = 19
#define ECLIB_fix16_20sr(par) \
	s16 par; \
	const s8 par##_sf = 20
#define ECLIB_fix16_21sr(par) \
	s16 par; \
	const s8 par##_sf = 21
#define ECLIB_fix16_22sr(par) \
	s16 par; \
	const s8 par##_sf = 22
#define ECLIB_fix16_23sr(par) \
	s16 par; \
	const s8 par##_sf = 23
#define ECLIB_fix16_24sr(par) \
	s16 par; \
	const s8 par##_sf = 24
#define ECLIB_fix16_25sr(par) \
	s16 par; \
	const s8 par##_sf = 25
#define ECLIB_fix16_26sr(par) \
	s16 par; \
	const s8 par##_sf = 26
#define ECLIB_fix16_27sr(par) \
	s16 par; \
	const s8 par##_sf = 27
#define ECLIB_fix16_28sr(par) \
	s16 par; \
	const s8 par##_sf = 28
#define ECLIB_fix16_29sr(par) \
	s16 par; \
	const s8 par##_sf = 29
#define ECLIB_fix16_30sr(par) \
	s16 par; \
	const s8 par##_sf = 30
#define ECLIB_fix16_31sr(par) \
	s16 par; \
	const s8 par##_sf = 31
#define ECLIB_fix16_32sr(par) \
	s16 par; \
	const s8 par##_sf = 32
#define ECLIB_fix16_33sr(par) \
	s16 par; \
	const s8 par##_sf = 33
#define ECLIB_fix16_34sr(par) \
	s16 par; \
	const s8 par##_sf = 34
#define ECLIB_fix16_35sr(par) \
	s16 par; \
	const s8 par##_sf = 35
#define ECLIB_fix16_36sr(par) \
	s16 par; \
	const s8 par##_sf = 36
#define ECLIB_fix16_37sr(par) \
	s16 par; \
	const s8 par##_sf = 37
#define ECLIB_fix16_38sr(par) \
	s16 par; \
	const s8 par##_sf = 38
#define ECLIB_fix16_39sr(par) \
	s16 par; \
	const s8 par##_sf = 39
#define ECLIB_fix16_40sr(par) \
	s16 par; \
	const s8 par##_sf = 40
#define ECLIB_fix16_41sr(par) \
	s16 par; \
	const s8 par##_sf = 41
#define ECLIB_fix16_42sr(par) \
	s16 par; \
	const s8 par##_sf = 42

#define ECLIB_fix16_0sl(par) \
	s16 par; \
	const s8 par##_sf = 0
#define ECLIB_fix16_1sl(par) \
	s16 par; \
	const s8 par##_sf = -1
#define ECLIB_fix16_2sl(par) \
	s16 par; \
	const s8 par##_sf = -2
#define ECLIB_fix16_3sl(par) \
	s16 par; \
	const s8 par##_sf = -3
#define ECLIB_fix16_4sl(par) \
	s16 par; \
	const s8 par##_sf = -4
#define ECLIB_fix16_5sl(par) \
	s16 par; \
	const s8 par##_sf = -5
#define ECLIB_fix16_6sl(par) \
	s16 par; \
	const s8 par##_sf = -6
#define ECLIB_fix16_7sl(par) \
	s16 par; \
	const s8 par##_sf = -7
#define ECLIB_fix16_8sl(par) \
	s16 par; \
	const s8 par##_sf = -8
#define ECLIB_fix16_9sl(par) \
	s16 par; \
	const s8 par##_sf = -9
#define ECLIB_fix16_10sl(par) \
	s16 par; \
	const s8 par##_sf = -10
#define ECLIB_fix16_11sl(par) \
	s16 par; \
	const s8 par##_sf = -11
#define ECLIB_fix16_12sl(par) \
	s16 par; \
	const s8 par##_sf = -12
#define ECLIB_fix16_13sl(par) \
	s16 par; \
	const s8 par##_sf = -13
#define ECLIB_fix16_14sl(par) \
	s16 par; \
	const s8 par##_sf = -14
#define ECLIB_fix16_15sl(par) \
	s16 par; \
	const s8 par##_sf = -15
#define ECLIB_fix16_16sl(par) \
	s16 par; \
	const s8 par##_sf = -16
#define ECLIB_fix16_17sl(par) \
	s16 par; \
	const s8 par##_sf = -17
#define ECLIB_fix16_18sl(par) \
	s16 par; \
	const s8 par##_sf = -18
#define ECLIB_fix16_19sl(par) \
	s16 par; \
	const s8 par##_sf = -19
#define ECLIB_fix16_20sl(par) \
	s16 par; \
	const s8 par##_sf = -20
#define ECLIB_fix16_21sl(par) \
	s16 par; \
	const s8 par##_sf = -21
#define ECLIB_fix16_22sl(par) \
	s16 par; \
	const s8 par##_sf = -22
#define ECLIB_fix16_23sl(par) \
	s16 par; \
	const s8 par##_sf = -23
#define ECLIB_fix16_24sl(par) \
	s16 par; \
	const s8 par##_sf = -24
#define ECLIB_fix16_25sl(par) \
	s16 par; \
	const s8 par##_sf = -25
#define ECLIB_fix16_26sl(par) \
	s16 par; \
	const s8 par##_sf = -26
#define ECLIB_fix16_27sl(par) \
	s16 par; \
	const s8 par##_sf = -27
#define ECLIB_fix16_28sl(par) \
	s16 par; \
	const s8 par##_sf = -28
#define ECLIB_fix16_29sl(par) \
	s16 par; \
	const s8 par##_sf = -29
#define ECLIB_fix16_30sl(par) \
	s16 par; \
	const s8 par##_sf = -30
#define ECLIB_fix16_31sl(par) \
	s16 par; \
	const s8 par##_sf = -31
#define ECLIB_fix16_32sl(par) \
	s16 par; \
	const s8 par##_sf = -32
#define ECLIB_fix16_33sl(par) \
	s16 par; \
	const s8 par##_sf = -33
#define ECLIB_fix16_34sl(par) \
	s16 par; \
	const s8 par##_sf = -34
#define ECLIB_fix16_35sl(par) \
	s16 par; \
	const s8 par##_sf = -35
#define ECLIB_fix16_36sl(par) \
	s16 par; \
	const s8 par##_sf = -36
#define ECLIB_fix16_37sl(par) \
	s16 par; \
	const s8 par##_sf = -37
#define ECLIB_fix16_38sl(par) \
	s16 par; \
	const s8 par##_sf = -38
#define ECLIB_fix16_39sl(par) \
	s16 par; \
	const s8 par##_sf = -39
#define ECLIB_fix16_40sl(par) \
	s16 par; \
	const s8 par##_sf = -40
#define ECLIB_fix16_41sl(par) \
	s16 par; \
	const s8 par##_sf = -41
#define ECLIB_fix16_42sl(par) \
	s16 par; \
	const s8 par##_sf = -42





#define ECLIB_struct_fix16(par) \
	s16 par; \
	const s8 par##_sf


#define ECLIB_extern_fix16(par) \
	extern s16 par; \
	extern const s8 par##_sf


#define ECLIB_static_fix16_0sr(par) \
		static s16 par ; \
		static  s8 par##_sf=0
#define ECLIB_static_fix16_1sr(par) \
		static s16 par ; \
		static  s8 par##_sf=1
#define ECLIB_static_fix16_2sr(par) \
		static s16 par ; \
		static  s8 par##_sf=2
#define ECLIB_static_fix16_3sr(par) \
		static s16 par ; \
		static  s8 par##_sf=3
#define ECLIB_static_fix16_4sr(par) \
		static s16 par ; \
		static  s8 par##_sf=4
#define ECLIB_static_fix16_5sr(par) \
		static s16 par ; \
		static  s8 par##_sf=5
#define ECLIB_static_fix16_6sr(par) \
		static s16 par ; \
		static  s8 par##_sf=6
#define ECLIB_static_fix16_7sr(par) \
		static s16 par ; \
		static  s8 par##_sf=7
#define ECLIB_static_fix16_8sr(par) \
		static s16 par ; \
		static  s8 par##_sf=8
#define ECLIB_static_fix16_9sr(par) \
		static s16 par ; \
		static  s8 par##_sf=9
#define ECLIB_static_fix16_10sr(par) \
		static s16 par ; \
		static  s8 par##_sf=10
#define ECLIB_static_fix16_11sr(par) \
		static s16 par ; \
		static  s8 par##_sf=11
#define ECLIB_static_fix16_12sr(par) \
		static s16 par ; \
		static  s8 par##_sf=12
#define ECLIB_static_fix16_13sr(par) \
		static s16 par ; \
		static  s8 par##_sf=13
#define ECLIB_static_fix16_14sr(par) \
		static s16 par ; \
		static  s8 par##_sf=14
#define ECLIB_static_fix16_15sr(par) \
		static s16 par ; \
		static  s8 par##_sf=15
#define ECLIB_static_fix16_16sr(par) \
		static s16 par ; \
		static  s8 par##_sf=16
#define ECLIB_static_fix16_17sr(par) \
		static s16 par ; \
		static  s8 par##_sf=17
#define ECLIB_static_fix16_18sr(par) \
		static s16 par ; \
		static  s8 par##_sf=18
#define ECLIB_static_fix16_19sr(par) \
		static s16 par ; \
		static  s8 par##_sf=19
#define ECLIB_static_fix16_20sr(par) \
		static s16 par ; \
		static  s8 par##_sf=20
#define ECLIB_static_fix16_21sr(par) \
		static s16 par ; \
		static  s8 par##_sf=21
#define ECLIB_static_fix16_22sr(par) \
		static s16 par ; \
		static  s8 par##_sf=22
#define ECLIB_static_fix16_23sr(par) \
		static s16 par ; \
		static  s8 par##_sf=23
#define ECLIB_static_fix16_24sr(par) \
		static s16 par ; \
		static  s8 par##_sf=24
#define ECLIB_static_fix16_25sr(par) \
		static s16 par ; \
		static  s8 par##_sf=25
#define ECLIB_static_fix16_26sr(par) \
		static s16 par ; \
		static  s8 par##_sf=26
#define ECLIB_static_fix16_27sr(par) \
		static s16 par ; \
		static  s8 par##_sf=27
#define ECLIB_static_fix16_28sr(par) \
		static s16 par ; \
		static  s8 par##_sf=28
#define ECLIB_static_fix16_29sr(par) \
		static s16 par ; \
		static  s8 par##_sf=29
#define ECLIB_static_fix16_30sr(par) \
		static s16 par ; \
		static  s8 par##_sf=30
#define ECLIB_static_fix16_31sr(par) \
		static s16 par ; \
		static  s8 par##_sf=31
#define ECLIB_static_fix16_32sr(par) \
		static s16 par ; \
		static  s8 par##_sf=32
#define ECLIB_static_fix16_33sr(par) \
		static s16 par ; \
		static  s8 par##_sf=33
#define ECLIB_static_fix16_34sr(par) \
		static s16 par ; \
		static  s8 par##_sf=34
#define ECLIB_static_fix16_35sr(par) \
		static s16 par ; \
		static  s8 par##_sf=35
#define ECLIB_static_fix16_36sr(par) \
		static s16 par ; \
		static  s8 par##_sf=36
#define ECLIB_static_fix16_37sr(par) \
		static s16 par ; \
		static  s8 par##_sf=37
#define ECLIB_static_fix16_38sr(par) \
		static s16 par ; \
		static  s8 par##_sf=38
#define ECLIB_static_fix16_39sr(par) \
		static s16 par ; \
		static  s8 par##_sf=39
#define ECLIB_static_fix16_40sr(par) \
		static s16 par ; \
		static  s8 par##_sf=40
#define ECLIB_static_fix16_41sr(par) \
		static s16 par ; \
		static  s8 par##_sf=41
#define ECLIB_static_fix16_42sr(par) \
		static s16 par ; \
		static  s8 par##_sf=42


#define ECLIB_static_fix16_0sl(par) \
		static s16 par ; \
		static  s8 par##_sf=0
#define ECLIB_static_fix16_1sl(par) \
		static s16 par ; \
		static  s8 par##_sf= -1
#define ECLIB_static_fix16_2sl(par) \
		static s16 par ; \
		static  s8 par##_sf= -2
#define ECLIB_static_fix16_3sl(par) \
		static s16 par ; \
		static  s8 par##_sf= -3
#define ECLIB_static_fix16_4sl(par) \
		static s16 par ; \
		static  s8 par##_sf= -4
#define ECLIB_static_fix16_5sl(par) \
		static s16 par ; \
		static  s8 par##_sf= -5
#define ECLIB_static_fix16_6sl(par) \
		static s16 par ; \
		static  s8 par##_sf= -6
#define ECLIB_static_fix16_7sl(par) \
		static s16 par ; \
		static  s8 par##_sf= -7
#define ECLIB_static_fix16_8sl(par) \
		static s16 par ; \
		static  s8 par##_sf= -8
#define ECLIB_static_fix16_9sl(par) \
		static s16 par ; \
		static  s8 par##_sf= -9
#define ECLIB_static_fix16_10sl(par) \
		static s16 par ; \
		static  s8 par##_sf= -10
#define ECLIB_static_fix16_11sl(par) \
		static s16 par ; \
		static  s8 par##_sf= -11
#define ECLIB_static_fix16_12sl(par) \
		static s16 par ; \
		static  s8 par##_sf= -12
#define ECLIB_static_fix16_13sl(par) \
		static s16 par ; \
		static  s8 par##_sf= -13
#define ECLIB_static_fix16_14sl(par) \
		static s16 par ; \
		static  s8 par##_sf= -14
#define ECLIB_static_fix16_15sl(par) \
		static s16 par ; \
		static  s8 par##_sf= -15
#define ECLIB_static_fix16_16sl(par) \
		static s16 par ; \
		static  s8 par##_sf= -16
#define ECLIB_static_fix16_17sl(par) \
		static s16 par ; \
		static  s8 par##_sf= -17
#define ECLIB_static_fix16_18sl(par) \
		static s16 par ; \
		static  s8 par##_sf= -18
#define ECLIB_static_fix16_19sl(par) \
		static s16 par ; \
		static  s8 par##_sf= -19
#define ECLIB_static_fix16_20sl(par) \
		static s16 par ; \
		static  s8 par##_sf= -20
#define ECLIB_static_fix16_21sl(par) \
		static s16 par ; \
		static  s8 par##_sf= -21
#define ECLIB_static_fix16_22sl(par) \
		static s16 par ; \
		static  s8 par##_sf= -22
#define ECLIB_static_fix16_23sl(par) \
		static s16 par ; \
		static  s8 par##_sf= -23
#define ECLIB_static_fix16_24sl(par) \
		static s16 par ; \
		static  s8 par##_sf= -24
#define ECLIB_static_fix16_25sl(par) \
		static s16 par ; \
		static  s8 par##_sf= -25
#define ECLIB_static_fix16_26sl(par) \
		static s16 par ; \
		static  s8 par##_sf= -26
#define ECLIB_static_fix16_27sl(par) \
		static s16 par ; \
		static  s8 par##_sf= -27
#define ECLIB_static_fix16_28sl(par) \
		static s16 par ; \
		static  s8 par##_sf= -28
#define ECLIB_static_fix16_29sl(par) \
		static s16 par ; \
		static  s8 par##_sf= -29
#define ECLIB_static_fix16_30sl(par) \
		static s16 par ; \
		static  s8 par##_sf= -30
#define ECLIB_static_fix16_31sl(par) \
		static s16 par ; \
		static  s8 par##_sf= -31
#define ECLIB_static_fix16_32sl(par) \
		static s16 par ; \
		static  s8 par##_sf= -32
#define ECLIB_static_fix16_33sl(par) \
		static s16 par ; \
		static  s8 par##_sf= -33
#define ECLIB_static_fix16_34sl(par) \
		static s16 par ; \
		static  s8 par##_sf= -34
#define ECLIB_static_fix16_35sl(par) \
		static s16 par ; \
		static  s8 par##_sf= -35
#define ECLIB_static_fix16_36sl(par) \
		static s16 par ; \
		static  s8 par##_sf= -36
#define ECLIB_static_fix16_37sl(par) \
		static s16 par ; \
		static  s8 par##_sf= -37
#define ECLIB_static_fix16_38sl(par) \
		static s16 par ; \
		static  s8 par##_sf= -38
#define ECLIB_static_fix16_39sl(par) \
		static s16 par ; \
		static  s8 par##_sf= -39
#define ECLIB_static_fix16_40sl(par) \
		static s16 par ; \
		static  s8 par##_sf= -40
#define ECLIB_static_fix16_41sl(par) \
		static s16 par ; \
		static  s8 par##_sf= -41
#define ECLIB_static_fix16_42sl(par) \
		static s16 par ; \
		static  s8 par##_sf= -42


/*
 *
 * Initialization for variables in structures and external variables
 *
 */


#define ECLIB_init_fix16_0sr(var)\
	*(s8*)&var##_sf = 0
#define ECLIB_init_fix16_1sr(var)\
	*(s8*)&var##_sf = 1
#define ECLIB_init_fix16_2sr(var)\
	*(s8*)&var##_sf = 2
#define ECLIB_init_fix16_3sr(var)\
	*(s8*)&var##_sf = 3
#define ECLIB_init_fix16_4sr(var)\
	*(s8*)&var##_sf = 4
#define ECLIB_init_fix16_5sr(var)\
	*(s8*)&var##_sf = 5
#define ECLIB_init_fix16_6sr(var)\
	*(s8*)&var##_sf = 6
#define ECLIB_init_fix16_7sr(var)\
	*(s8*)&var##_sf = 7
#define ECLIB_init_fix16_8sr(var)\
	*(s8*)&var##_sf = 8
#define ECLIB_init_fix16_9sr(var)\
	*(s8*)&var##_sf = 9
#define ECLIB_init_fix16_10sr(var)\
	*(s8*)&var##_sf = 10
#define ECLIB_init_fix16_11sr(var)\
	*(s8*)&var##_sf = 11
#define ECLIB_init_fix16_12sr(var)\
	*(s8*)&var##_sf = 12
#define ECLIB_init_fix16_13sr(var)\
	*(s8*)&var##_sf = 13
#define ECLIB_init_fix16_14sr(var)\
	*(s8*)&var##_sf = 14
#define ECLIB_init_fix16_15sr(var)\
	*(s8*)&var##_sf = 15
#define ECLIB_init_fix16_16sr(var)\
	*(s8*)&var##_sf = 16
#define ECLIB_init_fix16_17sr(var)\
	*(s8*)&var##_sf = 17
#define ECLIB_init_fix16_18sr(var)\
	*(s8*)&var##_sf = 18
#define ECLIB_init_fix16_19sr(var)\
	*(s8*)&var##_sf = 19
#define ECLIB_init_fix16_20sr(var)\
	*(s8*)&var##_sf = 20
#define ECLIB_init_fix16_21sr(var)\
	*(s8*)&var##_sf = 21
#define ECLIB_init_fix16_22sr(var)\
	*(s8*)&var##_sf = 22
#define ECLIB_init_fix16_23sr(var)\
	*(s8*)&var##_sf = 23
#define ECLIB_init_fix16_24sr(var)\
	*(s8*)&var##_sf = 24
#define ECLIB_init_fix16_25sr(var)\
	*(s8*)&var##_sf = 25
#define ECLIB_init_fix16_26sr(var)\
	*(s8*)&var##_sf = 26
#define ECLIB_init_fix16_27sr(var)\
	*(s8*)&var##_sf = 27
#define ECLIB_init_fix16_28sr(var)\
	*(s8*)&var##_sf = 28
#define ECLIB_init_fix16_29sr(var)\
	*(s8*)&var##_sf = 29
#define ECLIB_init_fix16_30sr(var)\
	*(s8*)&var##_sf = 30
#define ECLIB_init_fix16_31sr(var)\
	*(s8*)&var##_sf = 31
#define ECLIB_init_fix16_32sr(var)\
	*(s8*)&var##_sf = 32
#define ECLIB_init_fix16_33sr(var)\
	*(s8*)&var##_sf = 33
#define ECLIB_init_fix16_34sr(var)\
	*(s8*)&var##_sf = 34
#define ECLIB_init_fix16_35sr(var)\
	*(s8*)&var##_sf = 35
#define ECLIB_init_fix16_36sr(var)\
	*(s8*)&var##_sf = 36
#define ECLIB_init_fix16_37sr(var)\
	*(s8*)&var##_sf = 37
#define ECLIB_init_fix16_38sr(var)\
	*(s8*)&var##_sf = 38
#define ECLIB_init_fix16_39sr(var)\
	*(s8*)&var##_sf = 39
#define ECLIB_init_fix16_40sr(var)\
	*(s8*)&var##_sf = 40
#define ECLIB_init_fix16_41sr(var)\
	*(s8*)&var##_sf = 41
#define ECLIB_init_fix16_42sr(var)\
	*(s8*)&var##_sf = 42

#define ECLIB_init_fix16_0sl(var)\
	*(s8*)&var##_sf = 0
#define ECLIB_init_fix16_1sl(var)\
	*(s8*)&var##_sf = -1
#define ECLIB_init_fix16_2sl(var)\
	*(s8*)&var##_sf = -2
#define ECLIB_init_fix16_3sl(var)\
	*(s8*)&var##_sf = -3
#define ECLIB_init_fix16_4sl(var)\
	*(s8*)&var##_sf = -4
#define ECLIB_init_fix16_5sl(var)\
	*(s8*)&var##_sf = -5
#define ECLIB_init_fix16_6sl(var)\
	*(s8*)&var##_sf = -6
#define ECLIB_init_fix16_7sl(var)\
	*(s8*)&var##_sf = -7
#define ECLIB_init_fix16_8sl(var)\
	*(s8*)&var##_sf = -8
#define ECLIB_init_fix16_9sl(var)\
	*(s8*)&var##_sf = -9
#define ECLIB_init_fix16_10sl(var)\
	*(s8*)&var##_sf = -10
#define ECLIB_init_fix16_11sl(var)\
	*(s8*)&var##_sf = -11
#define ECLIB_init_fix16_12sl(var)\
	*(s8*)&var##_sf = -12
#define ECLIB_init_fix16_13sl(var)\
	*(s8*)&var##_sf = -13
#define ECLIB_init_fix16_14sl(var)\
	*(s8*)&var##_sf = -14
#define ECLIB_init_fix16_15sl(var)\
	*(s8*)&var##_sf = -15
#define ECLIB_init_fix16_16sl(var)\
	*(s8*)&var##_sf = -16
#define ECLIB_init_fix16_17sl(var)\
	*(s8*)&var##_sf = -17
#define ECLIB_init_fix16_18sl(var)\
	*(s8*)&var##_sf = -18
#define ECLIB_init_fix16_19sl(var)\
	*(s8*)&var##_sf = -19
#define ECLIB_init_fix16_20sl(var)\
	*(s8*)&var##_sf = -20
#define ECLIB_init_fix16_21sl(var)\
	*(s8*)&var##_sf = -21
#define ECLIB_init_fix16_22sl(var)\
	*(s8*)&var##_sf = -22
#define ECLIB_init_fix16_23sl(var)\
	*(s8*)&var##_sf = -23
#define ECLIB_init_fix16_24sl(var)\
	*(s8*)&var##_sf = -24
#define ECLIB_init_fix16_25sl(var)\
	*(s8*)&var##_sf = -25
#define ECLIB_init_fix16_26sl(var)\
	*(s8*)&var##_sf = -26
#define ECLIB_init_fix16_27sl(var)\
	*(s8*)&var##_sf = -27
#define ECLIB_init_fix16_28sl(var)\
	*(s8*)&var##_sf = -28
#define ECLIB_init_fix16_29sl(var)\
	*(s8*)&var##_sf = -29
#define ECLIB_init_fix16_30sl(var)\
	*(s8*)&var##_sf = -30
#define ECLIB_init_fix16_31sl(var)\
	*(s8*)&var##_sf = -31
#define ECLIB_init_fix16_32sl(var)\
	*(s8*)&var##_sf = -32
#define ECLIB_init_fix16_33sl(var)\
	*(s8*)&var##_sf = -33
#define ECLIB_init_fix16_34sl(var)\
	*(s8*)&var##_sf = -34
#define ECLIB_init_fix16_35sl(var)\
	*(s8*)&var##_sf = -35
#define ECLIB_init_fix16_36sl(var)\
	*(s8*)&var##_sf = -36
#define ECLIB_init_fix16_37sl(var)\
	*(s8*)&var##_sf = -37
#define ECLIB_init_fix16_38sl(var)\
	*(s8*)&var##_sf = -38
#define ECLIB_init_fix16_39sl(var)\
	*(s8*)&var##_sf = -39
#define ECLIB_init_fix16_40sl(var)\
	*(s8*)&var##_sf = -40
#define ECLIB_init_fix16_41sl(var)\
	*(s8*)&var##_sf = -41
#define ECLIB_init_fix16_42sl(var)\
	*(s8*)&var##_sf = -42


#endif /* SRC_EC16FIXEDPOINT_EC16DATATYPES_H_ */
