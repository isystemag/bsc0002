/*
 * ec16defines.h
 *
 *  Created on: 21.06.2016
 *      Author: KREUTZUL
 */

#ifndef EC16FIXEDPOINT_EC16DEFINES_H_
#define EC16FIXEDPOINT_EC16DEFINES_H_

#include "../common/ecdefines.h"


#ifdef ECLIB_DEVELOP
#define ECLIB_ASSERT(cond, msg)	{ if(!cond) { ECLIB_ReportError(msg, __FILE__, __LINE__); }
#else
#define ECLIB_ASSERT(cond, msg)
#endif



#define ECLIB_S8_MIN 			(s8)0x80			// -128   (10000000 in binary)
#define ECLIB_S8_MAX 			(s8)0x7F			// 127

#define ECLIB_S16_NAN	    	(s16)0x8000     // =-32768
#define ECLIB_S16_POS_INF  		(s16)0x7FFF		// = 32767
#define ECLIB_S16_NEG_INF		(s16)0x8001		// =-32767


#define ECLIB_S32_NAN		(s32)-2147483648 // (s32)0x80000000		// = -2147483648
#define ECLIB_S32_POS_INF  	(s32) 2147483647 // (s32)0x7FFFFFFF		// =  2147483647
#define ECLIB_S32_NEG_INF	(s32)-2147483647 // (s32)0x80000001		// = -2147483647

#endif /* EC16FIXEDPOINT_EC16DEFINES_H_ */
