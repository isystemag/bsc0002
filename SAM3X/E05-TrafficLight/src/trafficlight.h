/*******************************************************************************
 *
 *
 *
 ******************************************************************************/
 
#ifndef _TRAFFIC_LIGHT
#define _TRAFFIC_LIGHT

enum TrafficLight {
    LIGHT_ALL_OFF,
    LIGHT_RED,
    LIGHT_RED_AMBER,
    LIGHT_AMBER,
    LIGHT_GREEN,
    LIGHT_ERROR
};

void trafficLightReset(void);
void trafficLightNextState(void);
void trafficLightError(void);
TrafficLight getTrafficLight(void);

#endif