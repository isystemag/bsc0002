/*
 * ec16interpolation.h
 *
 *  Created on: 12.09.2016
 *      Author: KREUTZUL
 */

#ifndef EC16INCLUDES_EC16PRIVATE_EC16INTERPOLATION_H_
#define EC16INCLUDES_EC16PRIVATE_EC16INTERPOLATION_H_


#include "ec16basicoperation.h"
#include "ec16utilities.h"

void ECLIB_LinInt_16(ECLIB_rcv_fix16(*res), ECLIB_rcv_fix16 (input), const ECLIB_rcv_fix16(*x_values), const ECLIB_rcv_fix16(*y_values), u8 number_of_entries);

#define ECLIB_LIN_INT_16(res, input, x_values, y_values, number_of_entries) \
		ECLIB_LinInt_16( ECLIB_pass_fix16(&res), ECLIB_pass_fix16(input), ECLIB_pass_fix16(x_values), ECLIB_pass_fix16(y_values), number_of_entries)

#endif /* EC16INCLUDES_EC16PRIVATE_EC16INTERPOLATION_H_ */
