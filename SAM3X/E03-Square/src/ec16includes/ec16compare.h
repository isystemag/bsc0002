/*
 * ec16compare.h
 *
 *  Created on: 24.08.2016
 *      Author: KREUTZUL
 */

#ifndef EC16INCLUDES_EC16PRIVATE_EC16COMPARE_H_
#define EC16INCLUDES_EC16PRIVATE_EC16COMPARE_H_


#include "ec16utilities.h"
#include "ec16basicoperation.h"


#define ECLIB_IS_EQUAL_16(res, par1, par2) \
		res = ECLIB_IsEqual_16( ECLIB_pass_fix16(par1), ECLIB_pass_fix16(par2))

#define ECLIB_IS_LESS_16(res, par1, par2) \
		res = ECLIB_IsLess_16( ECLIB_pass_fix16(par1), ECLIB_pass_fix16(par2))

#define ECLIB_IS_LESS_OR_EQUAL_16(res, par1, par2) \
		res = ECLIB_IsLessOrEqual_16( ECLIB_pass_fix16(par1), ECLIB_pass_fix16(par2))

#define ECLIB_IS_GREATER_16(res, par1, par2) \
		res = ECLIB_IsGreater_16( ECLIB_pass_fix16(par1), ECLIB_pass_fix16(par2))

#define ECLIB_IS_GREATER_OR_EQUAL_16(res, par1, par2) \
		res = ECLIB_IsGreaterOrEqual_16( ECLIB_pass_fix16(par1), ECLIB_pass_fix16(par2))

ECLIB_Bool ECLIB_IsEqual_16( ECLIB_rcv_fix16(par1), ECLIB_rcv_fix16(par2));
ECLIB_Bool ECLIB_IsLess_16( ECLIB_rcv_fix16(par1), ECLIB_rcv_fix16(par2));
ECLIB_Bool ECLIB_IsLessOrEqual_16( ECLIB_rcv_fix16(par1), ECLIB_rcv_fix16(par2));
ECLIB_Bool ECLIB_IsGreater_16( ECLIB_rcv_fix16(par1), ECLIB_rcv_fix16(par2));
ECLIB_Bool ECLIB_IsGreaterOrEqual_16( ECLIB_rcv_fix16(par1), ECLIB_rcv_fix16(par2));


#define ECLIB_IS_NAN_16(res, par) \
		res = ECLIB_IsNAN_16( ECLIB_pass_fix16(par))

#define ECLIB_IS_POS_INF_16(res, par) \
		res = ECLIB_IsPOSINF_16( ECLIB_pass_fix16(par))

#define ECLIB_IS_NEG_INF_16(res, par) \
		res = ECLIB_IsNEGINF_16( ECLIB_pass_fix16(par))

ECLIB_Bool ECLIB_IsNAN_16( ECLIB_rcv_fix16(par)) ;
ECLIB_Bool ECLIB_IsPOSINF_16( ECLIB_rcv_fix16(par)) ;
ECLIB_Bool ECLIB_IsNEGINF_16( ECLIB_rcv_fix16(par));

#endif /* EC16INCLUDES_EC16PRIVATE_EC16COMPARE_H_ */
