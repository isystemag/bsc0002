/*
 * ecdatatypes.h
 *
 *  Created on: 19.04.2016
 *      Author: KREUTZUL
 */

#ifndef SRC_COMMON_ECDATATYPES_H_
#define SRC_COMMON_ECDATATYPES_H_


typedef long 			s32;
typedef unsigned long 	u32;
typedef short 			s16;
typedef unsigned short  u16;
typedef signed char  	s8;
typedef unsigned char  	u8;

typedef enum  {ECLIB_FALSE, ECLIB_TRUE} ECLIB_Bool;

#endif /* SRC_COMMON_ECDATATYPES_H_ */
